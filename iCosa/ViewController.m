//
//  ViewController.m
//  iCosa
//
//  Created by Franco Cedillo on 4/14/13.
//  Copyright (c) 2013 HartasCuerdas. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize boton, etiqueta;
@synthesize lblGreeting, txtName;


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)traducir
{
    etiqueta.text = [NSString stringWithFormat:@"Hello World!"];
}

-(IBAction)mensaje
{
    UIAlertView *alerta = [[UIAlertView alloc]initWithTitle:@"Hola      Mundo!" message:@"Hello World" delegate:nil cancelButtonTitle:@"Funciona!" otherButtonTitles:nil];
    [alerta show];
    [alerta release];
}

-(IBAction)saludar
{
    lblGreeting.text = [NSString stringWithFormat:@"Hola %@", txtName.text];
}

-(IBAction)esconderTeclado
{
    //Este método esconde el teclado al pulsar la tecla "Return" del mismo. El método estará vacio, pues la función de quitar el teclado vendra a raiz de como llamamos al metodo desde el campo de texto.
}


@end
