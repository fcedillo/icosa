//
//  ViewController.h
//  iCosa
//
//  Created by Franco Cedillo on 4/14/13.
//  Copyright (c) 2013 HartasCuerdas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {

  IBOutlet UILabel *etiqueta;
  IBOutlet UIButton *boton;
  IBOutlet UITextField *txtName;
  IBOutlet UILabel *lblGreeting;

}

@property (nonatomic, retain) IBOutlet UILabel *etiqueta;
@property (nonatomic, retain) IBOutlet UIButton *boton;
@property (nonatomic, retain) IBOutlet UILabel *lblGreeting;
@property (nonatomic, retain) IBOutlet UITextField *txtName;


-(IBAction)traducir;
-(IBAction)mensaje;
-(IBAction)saludar;
-(IBAction)esconderTeclado;

@end
