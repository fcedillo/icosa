//
//  main.m
//  iCosa
//
//  Created by Franco Cedillo on 4/14/13.
//  Copyright (c) 2013 HartasCuerdas. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
